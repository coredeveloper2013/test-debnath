<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;

class DeleteUnusedFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteUnusedFile:deletefile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Unused file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


      $media =  DB::table('media')->where('status', 0)->where('created_at', '<', Carbon::now()->addHours(4));
      foreach ($media as $file){
          $file->delete();
          unlink(base_path().'/storage/app/uploads/'.$file->media);
      }
    }
}
