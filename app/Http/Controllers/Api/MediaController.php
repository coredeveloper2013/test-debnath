<?php

namespace App\Http\Controllers\Api;

use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    protected $image_path;

    public function __construct()
    {
        $this->image_path = base_path().'/storage/app/uploads';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if($request->hasFile('media')){
            $image = $this->uploadFile($request->file('media'));
        }else{
            $image = null;
        }



         Media::create([
            'media'     => $image,
             'status' => 0
        ]);

        return response()->json('success');
    }

    public function uploadFile($image){

        $originalName = $image->getClientOriginalName();

        $new_name = $this->file_newname($this->image_path, $originalName);

        $image->move( $this->image_path, $new_name);
        return $new_name;
    }

    function file_newname($path, $filename){
        if ($pos = strrpos($filename, '.')) {
            $name = substr($filename, 0, $pos);
            $ext = substr($filename, $pos);
        } else {
            $name = $filename;
        }

        $newpath = $path.'/'.$filename;
        $newname = $filename;
        $counter = 0;
        while (file_exists($newpath)) {
            $newname = $name .'_'. $counter . $ext;
            $newpath = $path.'/'.$newname;
            $counter++;
        }

        return $newname;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $media = Media::find($id);
        if($media->status == 0){
            $media->status = 1;
            $media->save();
        }

        return response()->json([
            'media' => $media
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $media = Media::find($id);
        $media->delete();

       return response()->json('media delete success');
    }
}
